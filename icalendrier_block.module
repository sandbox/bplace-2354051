<?php
/**
 * @file
 * Light and simple calendar block displaying infos about current day
 */


/**
 * Displays help and module information.
 *
 * @param path
 *   Which path of the site we're using to display help
 * @param arg
 *   Array that holds the current path as returned from arg() function
 */
function icalendrier_block_help($path, $arg) {
    switch ($path) {
        case "admin/help#icalendrier_block":
            return '<p>' . t("Light and simple calendar block displaying infos about current day") . '</p>';
            break;
    }
}

/**
 * Implements hook_block_info().
 */
function icalendrier_block_block_info() {
    $blocks['icalendrier'] = array(
        'info' => t('iCalendrier'),
    );
    return $blocks;
}

/**
 * Implements hook_menu().
 */
function icalendrier_block_menu() {
    $items['icalendrier_block_ajax'] = array(
        'page callback' => 'icalendrier_block_ajax',
        'access arguments' => array('access content'),
        'type' => MENU_CALLBACK,
    );
    return $items;
}

/**
 * Implements hook_block_configure().
 */
function icalendrier_block_block_configure($delta = '') {
    // iCalendrier options
    $form = array();
    if ($delta == 'icalendrier') {
        $form['cal_type'] = array(
            '#type' => 'select',
            '#title' => t('Calendar type'),
            '#default_value' => variable_get('cal_type', 'comp175'),
            '#options' => array(
                'comp175' => t('Compact 175px'),
                'wide300' => t('Wide 300px'),
            ),
        );
        $form['language'] = array(
            '#type' => 'select',
            '#title' => t('Language'),
            '#default_value' => variable_get('language', 'en'),
            '#options' => array(
                'de' => 'Deutsch',
                'en' => 'English',
                'es' => 'Español',
                'fr' => 'Français',
                'it' => 'Italiano',
                'pt' => 'Português',
                'pt-BR' => 'Português (Brasil)',
            ),
        );
        $form['timezone'] = array(
            '#type' => 'select',
            '#title' => t('Timezone'),
            '#default_value' => variable_get('timezone', '0'),
            '#options' => array(
                '0' => t('Automatic'),
                'Europe/Paris' => 'Europe/Paris',
                'Europe/Berlin' => 'Europe/Berlin',
                'Europe/Moscow' => 'Europe/Moscow',
                'America/Montreal' => 'America/Montreal',
                'America/Guyana' => 'America/Guyana',
                'UTC' => 'UTC',
            ),
        );
        $form['bg_color'] = array(
            '#type' => 'textfield',
            '#title' => t('Custom Background Color'),
            '#default_value' => variable_get('bg_color', false),
        );
        $form['show_link'] = array(
            '#type' => 'checkbox',
            '#title' => t('Display link'),
            '#default_value' => variable_get('show_link', true),
        );
    }
    return $form;
}



function icalendrier_block_block_save($delta = '', $edit = array()) {
    if ($delta == 'icalendrier') {
        variable_set('cal_type', $edit['cal_type']);
        variable_set('language', $edit['language']);
        variable_set('timezone', $edit['timezone']);
        variable_set('bg_color', isset($edit['bg_color'])?$edit['bg_color']:false);
        variable_set('show_link', $edit['show_link']);
    }
}



/**
 * Implements hook_block_view().
 */
function icalendrier_block_block_view($delta) {
    $icalendrier_block_language = variable_get('language', 'en');
    $icalendrier_block_timezone = variable_get('timezone', '0');
    $icalendrier_block_cal_type = variable_get('cal_type', '0');
    $icalendrier_block_bg_color = variable_get('bg_color', false);
    $icalendrier_block_show_link = variable_get('show_link', false);
    $icalendrier_block_markup = "";

    $icalendrier_block_iCalendrier = new Icalendrier($icalendrier_block_language, $icalendrier_block_timezone);


    if('comp175' === $icalendrier_block_cal_type) {
        $icalendrier_block_markup = $icalendrier_block_iCalendrier->iCalendrierComp($icalendrier_block_show_link, $icalendrier_block_bg_color);
    } else if ('wide300' === $icalendrier_block_cal_type) {
        $icalendrier_block_markup = $icalendrier_block_iCalendrier->iCalendrierWide($icalendrier_block_show_link, $icalendrier_block_bg_color);
    }

    return array(
        'subject' => t('iCalendrier'),
        'content' => array(
            '#markup' => $icalendrier_block_markup,
        ),
    );
}
