-- SUMMARY --

Simple plugin that displays daily informations like date, name day or moon phase.

For a full description of the module, visit the project page:
  http://drupal.org/project/icalendrier_block

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/icalendrier_block


-- REQUIREMENTS --

* Drupal 7.x
* PHP 5.2 or higher


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* You will be able to enable the block in Structure > Blocks. Don't forget to
  change the language, it's not automatic!


-- CONFIGURATION --

* Block title: usual configuration option.

* Calendar type: currently two types available, wide will adjust its size to
  your design.

* Language: select the calendar language here, Deutsch, English, Español,
  Français, Italiano, Português and Português (Brasil) available.

* Timezone: leave to automatic, only few timezone are in the list.

* Background color: leave empty for default, or fill in a CSS color.

* Display link: if checked, add a link to the calendar site (compact only).


-- CUSTOMIZATION --

* Background color can be set in the configuration, but you can customize
  everything in css/icalendrier.css.

-- TROUBLESHOOTING --

* The wide type calendar do not have a background color set, it may look
  weird in your site design. Set the background color to "#fff" to make
  the background white.


-- FAQ --

Q: Where is the FAQ?

A: No question asked ATM.


-- CONTACT --

Current maintainers:
* Baptiste Placé - https://www.drupal.org/u/bplace

This project was made for:
* iCalendrier and its international translations
  iCalendrier is a website dedicated to free calendar resources and infos
  about important dates in France. Several translations are available,
  check them on http://icalendrier.fr/ and http://icalendrier.fr/a-propos
